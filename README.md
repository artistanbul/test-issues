# Redmine Test Issues

This is an internal tool that we use to create separate test issues for development tasks. Requires Python 3.6

## Installation

```
pipenv --three install
cp configuration.example.py configuration.py
```

Edit `configuration.py` with the URL of the Redmine installation and your Redmine API key. You can obtain your API key from your profile page.

## Usage

You can use this tool by simply running:

```
pipenv run python redtask.py
```

You will also find that `--help` will always be given to those who ask for it.
