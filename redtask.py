import click
import configuration
import sys

from redminelib import Redmine
from redminelib.exceptions import ResourceAttrError
from termcolor import cprint


def get_redmine():
    return Redmine(configuration.redmine_url, key=configuration.api_key)


def get_projects(redmine):
    projects = redmine.project.all()

    projects_dict = {}
    for project in projects:
        if project.status != 1:
            continue
        projects_dict[project.id] = project

    return projects_dict


def print_projects(projects):
    prefix_length = max(map(lambda x: len(str(x)), projects.keys()))
    for project in projects.values():
        if not hasattr(project, 'parent'):
            prefix = ""
        elif project.parent.id == previous_project.id:
            prefix += "- "
        elif project.parent.id != previous_project.parent.id:
            prefix = prefix[:-2]
        previous_project = project
        print(f"[{project.id:{prefix_length}}] {prefix}{project.name}")


def find_test_tracker(project):
    return list(filter(
        lambda x: x.name == "Test",
        project.trackers
    ))[0]


def get_versions(project, status_filter='open'):
    versions_dict = {}
    for version in project.versions:
        if status_filter is not None and version.status != status_filter:
            continue

        versions_dict[version.id] = version

    return versions_dict


def get_issues(redmine, version):
    issues_dict = {}
    for issue in redmine.issue.filter(version_id=version.id, status_id='*'):
        issues_dict[issue.id] = issue

    return issues_dict


def get_statuses(redmine):
    statuses_dict = {}
    for status in redmine.issue_status.all():
        statuses_dict[status.id] = status

    return statuses_dict


def get_users(project):
    users = list(map(
        lambda x: x.user,
        filter(
            lambda x: hasattr(x, 'user'),
            project.memberships
        )
    ))

    users_dict = {}
    for user in users:
        users_dict[user.id] = user
    return users_dict


def print_title(title):
    print()
    cprint(title, 'yellow')
    cprint('-' * len(title), 'yellow')


def prompt(text: str, input_type=int, choices: list=None, force: bool=False):
    try:
        user_input = click.prompt(text, type=input_type)
        if choices is not None and user_input not in choices:
            cprint(f'Invalid input: {user_input}', 'red')
            if force:
                return prompt(text, input_type, choices, force)
            else:
                sys.exit(1)
        else:
            return user_input
    except click.exceptions.Abort:
        print()
        cprint('Aborting.', 'yellow')
        sys.exit(2)


def prompt_options(title: str, prompt_text: str, options: dict, name_attr: str= "name", print_function=None):
    print_title(title)
    if print_function is None:
        prefix_length = max(map(lambda x: len(str(x)), options.keys()))
        for option in options.values():
            name = getattr(option, name_attr)
            print(f"[{option.id:{prefix_length}}] {name}")
    else:
        print_function(options)
    selected = prompt(prompt_text, choices=options.keys(), force=True)
    return selected


@click.command()
@click.option('--project', type=click.INT)
@click.option('--version', type=click.INT)
@click.option('--user', type=click.INT)
@click.option('--parent-issue', type=click.INT)
@click.option('--status', type=click.INT)
def main(project, version, user, parent_issue, status):
    redmine = get_redmine()

    projects = get_projects(redmine)
    if project is None:
        project = prompt_options(
            'Your projects',
            'Choose a project',
            projects,
            print_function=print_projects
        )
    project = projects.get(project)

    try:
        tracker = find_test_tracker(project)
    except IndexError:
        cprint(f'Error: {project.name} project does not have the Test tracker. Aborting.', 'red')
        sys.exit(1)

    versions = get_versions(project)
    if version is None:
        version = prompt_options(
            f"Open versions in {project.name}",
            "Choose a version",
            versions
        )
    version = versions.get(version)

    users = get_users(project)
    if user is None:
        user = prompt_options(
            f'Members of project: {project.name}',
            'Assign issues to',
            users
        )
    user = users.get(user)

    issues = get_issues(redmine, version)
    selected_parent_issue = prompt_options(
        f'All issues in {version.name}',
        'Choose parent issue',
        issues,
        name_attr="subject"
    )
    parent_issue = issues.get(selected_parent_issue)
    selected_issues = prompt('Choose tasks to create test issues, separate multiple IDs with comma', input_type=str)
    development_issues = map(issues.get, map(int, map(str.strip, selected_issues.split(','))))

    statuses = get_statuses(redmine)
    if status is None:
        """
        selected_status = prompt_options(
            'Issue statuses',
            'Choose a state',
            statuses
        )
        """
        # Use "Kontrol Edilecek" status, instead of asking for it.
        status = 9
    status = statuses.get(status)

    for issue in development_issues:
        test_issue = redmine.issue.create(
            project_id=project.id,
            tracker_id=tracker.id,
            status_id=status.id,
            fixed_version_id=version.id,
            assigned_to_id=user.id,
            parent_issue_id=parent_issue.id,
            start_date=parent_issue.start_date,
            due_date=parent_issue.due_date,
            subject=f"[TEST] {issue.subject}"
        )
        redmine.issue_relation.create(
            issue_id=test_issue.id,
            issue_to_id=issue.id,
            relation_type='relates'
        )


if __name__ == "__main__":
    main()
